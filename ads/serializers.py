# -*- coding: utf-8 -*-
from rest_framework import serializers

from ads import models


class AdvertiserSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Advertiser
        fields = (
            'pk',
            'name',
            'last_name',
            'phone',
            'cell',
            'email',
        )


class SubCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.SubCategory
        fields = (
            'name',
        )


class CategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Category
        depth = 1
        fields = (
            'pk',
            'name',
            'icon',
            'subcategory_set'
        )


class AdSerializer(serializers.ModelSerializer):
    category = CategorySerializer()

    class Meta:
        model = models.Ad
        depth = 1
        fields = (
            'pk',
            'title',
            'trade_name',
            'description',
            'state',
            'phone',
            'cell',
            'date',
            'advertiser',
            'category',
            'video',
            'location',
            'adpicture_set'
        )


class AdPictureSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.AdPicture


class ContactSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Contact
        fields = (
            'pk',
            'address',
            'phone',
            'email',
        )
        

class AboutSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.About
        fields = (
            'pk',
            'about',
            'mission',
            'vision',
            'values'
        )


class CouponSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Coupon
        fields = (
            'name',
            'date',
            'coupon',
            'active'
        )


class BannerGroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.BannerGroup


class BannerSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Banner


class MobileBannerGroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.MobileBannerGroup
        depth = 1
        fields = (
            'mobilebanner_set',
        )


class MobileBannerSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.MobileBanner
        depth = 1
        fields = '__all__'
