# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os

from django.contrib.auth.models import User
from django.core import validators
from django.db import models
from django.dispatch import receiver
from django.utils import timezone
from django.utils.encoding import python_2_unicode_compatible
from geoposition.fields import GeopositionField

# region Constants
DURATION = 'Duración'
BASE_MODEL_CREATED = 'Creado'
BASE_MODEL_MODIFIED = 'Modificado'
BASE_MODE_CREATED_BY = 'Creado por'
BASE_MODE_MODIFIE_BY = 'Modificado por'
NAME = 'Nombre(s)'
LAST_NAME = 'Apellido(s)'
PHONE = 'Teléfono'
CELL = 'Celular'
TITLE = 'Título'
DESC = 'Descripción'
ADVERTISER = 'Anunciante'
ADVERTISERS = 'Anunciantes'
AD = 'Anuncio'
AD_LOCATION = 'Ubicación'
ADS = 'Anuncios'
AD_STATE = 'Estado'
IMAGE = 'Imagen'
IMAGES = 'Imágenes'
EMAIL = 'Correo electrónico'
TRADE_NAME = 'Nombre comercial'
COUPON = 'Cupón'
COUPONS = 'Cupones'
COUPON_NAME = 'Nombre'
COUPON_DATE = 'Fecha'
COUPON_ACTIVE = 'Activo'
CONTACT = 'Contacto'
CONTACTS = 'Contactos'
ABOUT = '¿Quiénes somos?'
MISSION = 'Misión'
VISION = 'Visión'
VALUES = 'Valores'
CONTACT_ADDRESS = 'Dirección'
CONTACT_COUNTRY = 'País'
CONTACT_PHONE = 'Teléfono'
CONTACT_EMAIL = 'Correo electrónico'
CATEGORY_NAME = 'Categoria'
CATEGORY_ICON = 'Icono'
SUBCATEGORY = 'Subcategoría'
SUBCATEGORIES = 'Subcategorías'
SUBCATEGORY_NAME = 'Nombre'
CATEGORY = 'Categoría'
CATEGORIES = 'Categorías'
BANNER = 'Banner'
BANNERS = 'Banners'
BANNERS_GROUP = 'Grupo de banners'
BANNERS_GROUPS = 'Grupos de banners'
BANNER_GROUP_NAME = 'Nombre'
BANNER_GROUP_ACTIVE = 'Activo'

MOBILE_BANNER = 'Banner para móvil'
MOBILES_BANNERS = 'Banners para móviles'
MOBILE_BANNERS_GROUP = 'Grupo de banners para móviles'
MOBILES_BANNERS_GROUPS = 'Grupos de banners para móviles'
MOBILE_BANNER_GROUP_NAME = 'Nombre'
MOBILE_BANNER_GROUP_ACTIVE = 'Activo'
# endregion

# region State choices
STATES = (
    ('Aguascalientes', 'Aguascalientes'),
    ('Baja California', 'Baja California'),
    ('Baja California Sur', 'Baja California Sur'),
    ('Campeche', 'Campeche'),
    ('Chihuahua', 'Chihuahua'),
    ('Chiapas', 'Chiapas'),
    ('Coahuila', 'Coahuila'),
    ('Colima', 'Colima'),
    ('Ciudad de México', 'Ciudad de México'),
    ('Durango', 'Durango'),
    ('Guerrero', 'Guerrero'),
    ('Guanajuato', 'Guanajuato'),
    ('Hidalgo', 'Hidalgo'),
    ('Jalisco', 'Jalisco'),
    ('Michoacán', 'Michoacán'),
    ('Morelos', 'Morelos'),
    ('Nayarit', 'Nayarit'),
    ('Nuevo León', 'Nuevo León'),
    ('Oaxaca', 'Oaxaca'),
    ('Puebla', 'Puebla'),
    ('Querétaro', 'Querétaro'),
    ('Quintana Roo', 'Quintana Roo'),
    ('Sinaloa', 'Sinaloa'),
    ('San Luis Potosí', 'San Luis Potosí'),
    ('Sonora', 'Sonora'),
    ('Tabasco', 'Tabasco'),
    ('Tamaulipas', 'Tamaulipas'),
    ('Tlaxcala', 'Tlaxcala'),
    ('Veracruz', 'Veracruz'),
    ('Yucatán', 'Yucatán'),
    ('Zacatecas', 'Zacatecas')
)
# endregion

# region Upload media folders.
UPLOAD_TO_IMAGES = 'images'
UPLOAD_TO_CATEGORY_ICONS = 'category_icons'
UPLOAD_TO_BANNERS = 'banners'
UPLOAD_TO_MOBILE_BANNERS = 'mobiles_banners'
# endregion

# Validators
PHONE_ERROR = 'Introduzca un número de teléfono valido'
phone_v = validators.RegexValidator(regex=r'^[0-9 -]*$', message=PHONE_ERROR)
NAME_ERROR = 'Este campo solo puede contener letras'
name_v = validators.RegexValidator(
    regex=r'^[a-zA-Z\u00C0-\u00D6\u00D8-\u00f6\u00f8-\u00ff ]*$',
    message=NAME_ERROR)


class BaseModel(models.Model):
    created = models.DateTimeField(
        auto_now_add=True,
        editable=False,
        verbose_name=BASE_MODEL_CREATED
    )
    modified = models.DateTimeField(
        auto_now=True,
        editable=False,
        verbose_name=BASE_MODEL_MODIFIED
    )

    class Meta:
        abstract = True


# Create your models here.
@python_2_unicode_compatible
class Advertiser(BaseModel):
    name = models.CharField(
        max_length=200,
        verbose_name=NAME,
        validators=[name_v]
    )
    last_name = models.CharField(
        max_length=200,
        verbose_name=LAST_NAME,
        validators=[name_v]
    )
    phone = models.CharField(
        max_length=50,
        blank=True,
        verbose_name=PHONE,
        validators=[phone_v]
    )
    cell = models.CharField(
        max_length=50,
        blank=True,
        verbose_name=CELL,
        validators=[phone_v]
    )
    email = models.EmailField(
        blank=True,
        verbose_name=EMAIL
    )
    created_by = models.ForeignKey(
        User,
        related_name='advertiser_created_by',
        verbose_name=BASE_MODE_CREATED_BY
    )
    modified_by = models.ForeignKey(
        User,
        related_name='advertiser_modifies_by',
        verbose_name=BASE_MODE_MODIFIE_BY
    )

    class Meta:
        verbose_name = ADVERTISER
        verbose_name_plural = ADVERTISERS

    def __str__(self):
        return '{0} {1}'.format(self.name, self.last_name)


@python_2_unicode_compatible
class Category(BaseModel):
    name = models.CharField(
        max_length=200,
        verbose_name=CATEGORY_NAME
    )
    icon = models.ImageField(
        upload_to=UPLOAD_TO_CATEGORY_ICONS,
        verbose_name=CATEGORY_ICON
    )
    created_by = models.ForeignKey(
        User,
        related_name='category_created_by',
        verbose_name=BASE_MODE_CREATED_BY
    )
    modified_by = models.ForeignKey(
        User,
        related_name='category_modifies_by',
        verbose_name=BASE_MODE_MODIFIE_BY
    )

    class Meta:
        verbose_name = CATEGORY
        verbose_name_plural = CATEGORIES
        ordering = ['name']

    def __str__(self):
        return '{0}'.format(self.name)


@receiver(models.signals.post_delete, sender=Category)
def auto_delete_category_on_delete(sender, instance, **kwargs):
    if instance.icon:
        if os.path.isfile(instance.icon.path):
            os.remove(instance.icon.path)


@receiver(models.signals.pre_save, sender=Category)
def auto_delete_category_on_change(sender, instance, **kwargs):
    if not instance.pk:
        return False
    try:
        old_file = Category.objects.get(pk=instance.pk).icon
    except Category.DoesNotExist:
        return False

    new_file = instance.icon
    if not old_file == new_file:
        if os.path.isfile(old_file.path):
            os.remove(old_file.path)


@python_2_unicode_compatible
class SubCategory(BaseModel):
    name = models.CharField(
        max_length=200,
        verbose_name=SUBCATEGORY_NAME
    )
    category = models.ForeignKey(
        Category,
        verbose_name=CATEGORY
    )
    created_by = models.ForeignKey(
        User,
        related_name='subcategory_created_by',
        verbose_name=BASE_MODE_CREATED_BY
    )
    modified_by = models.ForeignKey(
        User,
        related_name='subcategory_modifies_by',
        verbose_name=BASE_MODE_MODIFIE_BY
    )

    class Meta:
        verbose_name = SUBCATEGORY
        verbose_name_plural = SUBCATEGORIES
        ordering = ['name']

    def __str__(self):
        return '{0}'.format(self.name)


@python_2_unicode_compatible
class Ad(BaseModel):
    title = models.CharField(max_length=200, verbose_name=TITLE)
    trade_name = models.CharField(max_length=200, verbose_name=TRADE_NAME)
    description = models.TextField(blank=True, verbose_name=DESC)
    state = models.CharField(
        max_length=200,
        choices=STATES,
        verbose_name=AD_STATE
    )
    phone = models.CharField(
        max_length=30,
        verbose_name=PHONE,
        validators=[phone_v]
    )
    cell = models.CharField(
        max_length=30,
        blank=True,
        verbose_name=CELL,
        validators=[phone_v]
    )
    date = models.DateTimeField(auto_now_add=True)
    advertiser = models.ForeignKey(Advertiser, verbose_name=ADVERTISER)
    category = models.ForeignKey(Category, verbose_name=CATEGORY_NAME)
    video = models.TextField(max_length=300, blank=True, null=True)
    location = GeopositionField(verbose_name=AD_LOCATION)
    created_by = models.ForeignKey(
        User,
        related_name='ad_created_by',
        verbose_name=BASE_MODE_CREATED_BY
    )
    modified_by = models.ForeignKey(
        User,
        related_name='ad_modifies_by',
        verbose_name=BASE_MODE_MODIFIE_BY
    )

    class Meta:
        verbose_name = AD
        verbose_name_plural = ADS
        ordering = ['title', 'trade_name']

    def __str__(self):
        return self.title


class AdPicture(BaseModel):
    image = models.ImageField(upload_to=UPLOAD_TO_IMAGES)
    ad = models.ForeignKey(Ad, verbose_name=AD)
    created_by = models.ForeignKey(
        User,
        related_name='adpicture_created_by',
        verbose_name=BASE_MODE_CREATED_BY
    )
    modified_by = models.ForeignKey(
        User,
        related_name='adpicture_modifies_by',
        verbose_name=BASE_MODE_MODIFIE_BY
    )

    class Meta:
        verbose_name = IMAGE
        verbose_name_plural = IMAGES

    def __str__(self):
        return '{0}'.format(self.ad.title)


@receiver(models.signals.post_delete, sender=AdPicture)
def auto_delete_ad_picture_on_delete(sender, instance, **kwargs):
    if instance.image:
        if os.path.isfile(instance.image.path):
            os.remove(instance.image.path)


@receiver(models.signals.pre_save, sender=AdPicture)
def auto_delete_ad_picture_on_change(sender, instance, **kwargs):
    if not instance.pk:
        return False
    try:
        old_file = AdPicture.objects.get(pk=instance.pk).image
    except AdPicture.DoesNotExist:
        return False

    new_file = instance.image
    if not old_file == new_file:
        if os.path.isfile(old_file.path):
            os.remove(old_file.path)


@python_2_unicode_compatible
class Contact(BaseModel):
    address = models.CharField(
        max_length=200,
        verbose_name=CONTACT_ADDRESS
    )
    phone = models.CharField(
        max_length=50,
        verbose_name=CONTACT_PHONE,
        validators=[phone_v]
    )
    email = models.EmailField(
        verbose_name=CONTACT_EMAIL
    )
    created_by = models.ForeignKey(
        User,
        related_name='contact_created_by',
        verbose_name=BASE_MODE_CREATED_BY
    )
    modified_by = models.ForeignKey(
        User,
        related_name='contact_modifies_by',
        verbose_name=BASE_MODE_MODIFIE_BY
    )

    class Meta:
        verbose_name = CONTACT
        verbose_name_plural = CONTACT

    def __str__(self):
        return '{0}'.format(self.address)

    def save(self, *args, **kwargs):
        self.__class__.objects.exclude(id=self.id).delete()
        super(Contact, self).save(*args, **kwargs)

    def load(cls):
        try:
            return cls.objects.get()
        except cls.DoesNotExist:
            return cls()


@python_2_unicode_compatible
class About(BaseModel):
    about = models.TextField(verbose_name=ABOUT)
    mission = models.TextField(verbose_name=MISSION)
    vision = models.TextField(verbose_name=VISION)
    values = models.TextField(verbose_name=VALUES)
    created_by = models.ForeignKey(
        User,
        related_name='about_created_by',
        verbose_name=BASE_MODE_CREATED_BY
    )
    modified_by = models.ForeignKey(
        User,
        related_name='about_modifies_by',
        verbose_name=BASE_MODE_MODIFIE_BY

    )

    class Meta:
        verbose_name = ABOUT
        verbose_name_plural = ABOUT

    def __str__(self):
        return ABOUT

    def save(self, *args, **kwargs):
        self.__class__.objects.exclude(id=self.id).delete()
        super(About, self).save(*args, **kwargs)

    def load(cls):
        try:
            return cls.objects.get()
        except cls.DoesNotExist:
            return cls()


@python_2_unicode_compatible
class Coupon(BaseModel):
    name = models.CharField(max_length=100, verbose_name=COUPON_NAME)
    date = models.DateField(default=timezone.now, verbose_name=COUPON_DATE)
    coupon = models.ImageField(upload_to='coupons', verbose_name=COUPON)
    active = models.BooleanField(verbose_name=COUPON_ACTIVE)
    created_by = models.ForeignKey(
        User,
        related_name='coupon_created_by',
        verbose_name=BASE_MODE_CREATED_BY
    )
    modified_by = models.ForeignKey(
        User,
        related_name='coupon_modifies_by',
        verbose_name=BASE_MODE_MODIFIE_BY
    )

    class Meta:
        verbose_name = COUPON
        verbose_name_plural = COUPONS
        ordering = ['-date']

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if self.active:
            self.__class__.objects.exclude(id=self.id).update(active=False)
        super(Coupon, self).save(*args, **kwargs)


@receiver(models.signals.post_delete, sender=Coupon)
def auto_delete_coupon_on_delete(sender, instance, **kwargs):
    if instance.coupon:
        if os.path.isfile(instance.coupon.path):
            os.remove(instance.coupon.path)


@receiver(models.signals.pre_save, sender=Coupon)
def auto_delete_coupon_on_change(sender, instance, **kwargs):
    if not instance.pk:
        return False
    try:
        old_file = Coupon.objects.get(pk=instance.pk).coupon
    except Coupon.DoesNotExist:
        return False

    new_file = instance.coupon
    if not old_file == new_file:
        if os.path.isfile(old_file.path):
            os.remove(old_file.path)


@python_2_unicode_compatible
class BannerGroup(BaseModel):
    name = models.CharField(max_length=200, verbose_name=BANNER_GROUP_NAME)
    active = models.BooleanField(verbose_name=BANNER_GROUP_ACTIVE)
    created_by = models.ForeignKey(
        User,
        related_name='banner_group_created_by',
        verbose_name=BASE_MODE_CREATED_BY
    )
    modified_by = models.ForeignKey(
        User,
        related_name='banner_group_modifies_by',
        verbose_name=BASE_MODE_MODIFIE_BY
    )

    class Meta:
        verbose_name = BANNERS_GROUP
        verbose_name_plural = BANNERS_GROUPS
        ordering = ['active', 'name']

    def __str__(self):
        return '{0}'.format(self.name)

    def save(self, *args, **kwargs):
        if self.active:
            self.__class__.objects.exclude(id=self.id).update(active=False)
        super(BannerGroup, self).save(*args, **kwargs)


@python_2_unicode_compatible
class Banner(BaseModel):
    banner = models.ImageField(upload_to=UPLOAD_TO_BANNERS)
    banner_group = models.ForeignKey(BannerGroup)
    duration = models.PositiveIntegerField(
        default=1000,
        verbose_name='Duración'
    )
    created_by = models.ForeignKey(
        User,
        related_name='banner_banner_group_created_by',
        verbose_name=BASE_MODE_CREATED_BY
    )
    modified_by = models.ForeignKey(
        User,
        related_name='banner_banner_group_modifies_by',
        verbose_name=BASE_MODE_MODIFIE_BY
    )

    class Meta:
        verbose_name = BANNER
        verbose_name_plural = BANNERS

    def __str__(self):
        return BANNER


@receiver(models.signals.post_delete, sender=Banner)
def auto_delete_banner_on_delete(sender, instance, **kwargs):
    if instance.banner:
        if os.path.isfile(instance.banner.path):
            os.remove(instance.banner.path)


@receiver(models.signals.pre_save, sender=Banner)
def auto_delete_banner_on_change(sender, instance, **kwargs):
    if not instance.pk:
        return False
    try:
        old_file = Banner.objects.get(pk=instance.pk).banner
    except Banner.DoesNotExist:
        return False

    new_file = instance.banner
    if not old_file == new_file:
        if os.path.isfile(old_file.path):
            os.remove(old_file.path)


class MobileBannerGroup(BaseModel):
    name = models.CharField(
        max_length=200,
        verbose_name=MOBILE_BANNER_GROUP_NAME
    )
    active = models.BooleanField(verbose_name=MOBILE_BANNER_GROUP_ACTIVE)
    created_by = models.ForeignKey(
        User,
        related_name='mobile_banner_group_created_by',
        verbose_name=BASE_MODE_CREATED_BY
    )
    modified_by = models.ForeignKey(
        User,
        related_name='mobile_banner_group_modifies_by',
        verbose_name=BASE_MODE_MODIFIE_BY
    )

    class Meta:
        verbose_name = MOBILE_BANNERS_GROUP
        verbose_name_plural = MOBILES_BANNERS_GROUPS
        ordering = ['active', 'name']

    def __str__(self):
        return '{0}'.format(self.name)

    def save(self, *args, **kwargs):
        if self.active:
            self.__class__.objects.exclude(id=self.id).update(active=False)
        super(MobileBannerGroup, self).save(*args, **kwargs)


@python_2_unicode_compatible
class MobileBanner(BaseModel):
    banner = models.ImageField(upload_to=UPLOAD_TO_MOBILE_BANNERS)
    mobile_banner_group = models.ForeignKey(MobileBannerGroup)

    created_by = models.ForeignKey(
        User,
        related_name='mobile_banner_banner_group_created_by',
        verbose_name=BASE_MODE_CREATED_BY
    )
    modified_by = models.ForeignKey(
        User,
        related_name='mobile_banner_banner_group_modifies_by',
        verbose_name=BASE_MODE_MODIFIE_BY
    )

    class Meta:
        verbose_name = MOBILE_BANNER
        verbose_name_plural = MOBILES_BANNERS

    def __str__(self):
        return MOBILE_BANNER


@receiver(models.signals.post_delete, sender=MobileBanner)
def auto_delete_banner_on_delete(sender, instance, **kwargs):
    if instance.banner:
        if os.path.isfile(instance.banner.path):
            os.remove(instance.banner.path)


@receiver(models.signals.pre_save, sender=MobileBanner)
def auto_delete_banner_on_change(sender, instance, **kwargs):
    if not instance.pk:
        return False
    try:
        old_file = MobileBanner.objects.get(pk=instance.pk).banner
    except MobileBanner.DoesNotExist:
        return False

    new_file = instance.banner
    if not old_file == new_file:
        if os.path.isfile(old_file.path):
            os.remove(old_file.path)
