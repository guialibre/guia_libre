# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import json

from django.core import serializers
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404
from django.template.loader import render_to_string
from django.utils.datastructures import MultiValueDictKeyError
from django.views.generic import ListView
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response

from ads.models import Contact, Coupon, About, Category, Ad, BannerGroup, \
    MobileBanner, MobileBannerGroup
# Create your views here.
from ads.serializers import AdSerializer, CouponSerializer, ContactSerializer, \
    AboutSerializer, CategorySerializer, MobileBannerSerializer, \
    MobileBannerGroupSerializer


def index(request):
    cat = Category.objects.all()
    banners = BannerGroup.objects.filter(active=True).first()

    # Get most recent ads.
    recent_ads = Ad.objects.order_by('-date').all()[:5]

    context = {
        'categories': cat,
        'banner_group': banners,
        'recent_ads': recent_ads
    }
    return render(request, 'index.html', context=context)


def contact(request):
    con = Contact.objects.all().first()
    return render(request, 'contact.html', context={'contact': con})


def coupons(request):
    coupon = Coupon.objects.filter(active=True).first()
    return render(request, 'coupons.html', context={'coupon': coupon})


def about_us(request):
    about = About.objects.first()
    return render(request, 'about.html', context={'about_us': about})


def categories(request):
    cat = Category.objects.all()
    return render(request, 'categories.html', context={'categories': cat})


def search(request):
    # GET PARAMETER FOR FILTER MODEL
    try:
        category_id = int(request.GET.get('category'))
    except (ValueError, TypeError):
        category_id = 0
    state = request.GET.get('state', default=None)
    if len(state) < 1:
        state = None

    keyword = request.GET.get('keyword', default=None)

    if len(keyword) > 0:
        # BUILD Q OBJECT QUERIES
        queries = Q(title__icontains=keyword) | \
                  Q(trade_name__icontains=keyword) | \
                  Q(description__icontains=keyword)
        if category_id > 0:
            queries = queries & Q(category_id=category_id)

        if state is not None and len(state) > 0:
            queries = queries & Q(state__icontains=state)

        ads = Ad.objects.filter(queries).all()
        resp = {'ads': ads, 'search': True, 'keyword': keyword}
        return render(request, 'ads.html', context=resp)


def get_categories(request):
    cat = Category.objects.all()
    j = serializers.serialize('json', cat)

    return HttpResponse(j, content_type='json')


def get_states(request):
    state_list = [
        {'state': 'Aguascalientes'},
        {'state': 'Baja California'},
        {'state': 'Baja California Sur'},
        {'state': 'Campeche'},
        {'state': 'Chihuahua'},
        {'state': 'Chiapas'},
        {'state': 'Coahuila'},
        {'state': 'Colima'},
        {'state': 'Ciudad de México'},
        {'state': 'Durango'},
        {'state': 'Guerrero'},
        {'state': 'Guanajuato'},
        {'state': 'Hidalgo'},
        {'state': 'Jalisco'},
        {'state': 'Estado de México'},
        {'state': 'Michoacán'},
        {'state': 'Morelos'},
        {'state': 'Nayarit'},
        {'state': 'Nuevo León'},
        {'state': 'Oaxaca'},
        {'state': 'Puebla'},
        {'state': 'Querétaro'},
        {'state': 'Quintana Roo'},
        {'state': 'Sinaloa'},
        {'state': 'San Luis Potosí'},
        {'state': 'Sonora'},
        {'state': 'Tabasco'},
        {'state': 'Tamaulipas'},
        {'state': 'Tlaxcala'},
        {'state': 'Veracruz'},
        {'state': 'Yucatán'},
        {'state': 'Zacatecas'}]
    return HttpResponse(json.dumps(state_list), content_type='json')


def ads(request):
    ads = Ad.objects.all()
    html = render_to_string('ads.html', {'ads': ads})
    return HttpResponse(html)


def ad_detail(request, pk):
    ad = get_object_or_404(Ad, pk=pk)
    return render(request, 'ad_detail.html', {'ad': ad})


class CategoriesListView(ListView):
    model = Category
    template_name = 'ads_by_category.html'
    paginate_by = 20

    def get_context_data(self, **kwargs):
        context = super(CategoriesListView, self).get_context_data(**kwargs)
        category_id = self.kwargs['pk']
        ads = Ad.objects.filter(category_id=category_id)
        paginator = Paginator(ads, self.paginate_by)

        page = self.request.GET.get('page')

        try:
            file_ads = paginator.page(page)
        except PageNotAnInteger:
            file_ads = paginator.page(1)
        except EmptyPage:
            file_ads = paginator.page(paginator.num_pages)

        context['list_ads'] = file_ads
        category_values = Category.objects.filter(pk=category_id).values('name')
        context['category_name'] = category_values[0]['name']
        return context


# API METHOD CONSTANTS
GET = 'GET'
POST = 'POST'


@api_view([GET, POST])
def ad_list(request, format=None):
    if request.method == 'GET':
        ads = Ad.objects.all()
        serializer = AdSerializer(ads, many=True)
        return Response(serializer.data)
    else:
        return Response(status=status.HTTP_400_BAD_REQUEST)


@api_view([GET])
def search_ad(request, keyword=None, format=None):
    if request.method == GET:
        try:
            keyword = request.query_params['keyword']
        except MultiValueDictKeyError:
            return Response(
                "Must send at least 'keyword' param",
                status=status.HTTP_400_BAD_REQUEST)

        try:
            state = request.query_params['state']
        except MultiValueDictKeyError:
            state = None

        try:
            category_id = request.query_params['category_id']
        except MultiValueDictKeyError:
            category_id = 0

        # BUILD Q OBJECT QUERIES
        queries = Q(title__icontains=keyword) |\
                  Q(trade_name__icontains=keyword) |\
                  Q(description__icontains=keyword)

        if category_id > 0:
            queries = queries & Q(category_id=category_id)

        if state is not None and len(state) > 0:
            queries = queries & Q(state__icontains=state)

        ads = Ad.objects.filter(queries).all()

        # Get pagination size
        try:
            page_size = request.query_params['page_size']
        except MultiValueDictKeyError:
            page_size = 2

        paginator = PageNumberPagination()
        paginator.page_size = page_size
        result_page = paginator.paginate_queryset(ads, request)

        serializer = AdSerializer(result_page, many=True)
        return paginator.get_paginated_response(serializer.data)
    else:
        return Response(status=status.HTTP_400_BAD_REQUEST)


@api_view([GET])
def get_coupon(request):
    if request.method == GET:
        coupon = Coupon.objects.filter(active=True).first()
        if coupon:
            serializer = CouponSerializer(coupon)
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(
                "Not active coupon found.",
                status=status.HTTP_404_NOT_FOUND
            )
    else:
        return Response(status.HTTP_400_BAD_REQUEST)


@api_view([GET])
def get_info(request):
    if request.method == GET:
        contact = Contact.objects.all().first()
        about = About.objects.all().first()
        if contact:
            contact_serializer = ContactSerializer(contact)
        if about:
            about_serializer = AboutSerializer(about)
        data = {
            'contact': contact_serializer.data if contact else None,
            'about': about_serializer.data if about else None
        }
        return Response(data, status=status.HTTP_200_OK)
    else:
        return Response(status=status.HTTP_400_BAD_REQUEST)


@api_view([GET])
def get_categories_api(request):
    if request.method == GET:
        categories = Category.objects.all()
        print(categories)
        serializer = CategorySerializer(categories, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
    else:
        return Response(status=status.HTTP_400_BAD_REQUEST)


@api_view([GET])
def get_most_recent(request):
    if request.method == GET:
        try:
            page_size = request.query_params['page_size']
        except MultiValueDictKeyError:
            page_size = 5

        ads = Ad.objects.order_by('-date').all()[:page_size]
        serializer = AdSerializer(ads, many=True)

        return Response(serializer.data, status=status.HTTP_200_OK)
    else:
        return Response(status=status.HTTP_400_BAD_REQUEST)


@api_view([GET])
def get_mobile_banner(request):
    if request.method == GET:
        banner = MobileBannerGroup.objects.filter(active=True).first()
        if banner:
            serializer = MobileBannerGroupSerializer(banner)
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(
                "No active mobile banner found.",
                status=status.HTTP_404_NOT_FOUND
            )
