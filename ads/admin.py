# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from ads import models

BANNERS_HELP_TEXT = """
Para una mejor visualización los banners deben tener una proporción de 16:9.
O ser más anchos que altos."""

base_read_only_fields = ['created_by', 'created', 'modified_by', 'modified']


# Register your models here.
class BaseAdmin(admin.ModelAdmin):
    def save_model(self, request, instance, form, change):
        user = request.user
        instance = form.save(commit=False)
        if not change or not instance.created_by:
            instance.created_by = user
        instance.modified_by = user
        instance.save()
        form.save_m2m()
        return instance

    def get_queryset(self, request):
        """Limit Pages to those that belong to the request's user."""
        qs = super(BaseAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            # It is mine, all mine. Just return everything.
            return qs
        # Now we just add an extra filter on the queryset and
        # we're done. Assumption: Page.owner is a foreignkey
        # to a User.
        return qs.filter(created_by=request.user)


class AdvertiserAdmin(BaseAdmin):
    # fields = ['name', 'last_name', 'phone', 'cell']
    list_filter = ['name', 'last_name', 'email', 'phone', 'cell']
    list_display = ['name', 'last_name', 'email', 'phone', 'cell']
    readonly_fields = base_read_only_fields


class SubCategoryInline(admin.TabularInline):
    model = models.SubCategory
    extra = 0
    readonly_fields = base_read_only_fields


class SubCategoryAdmin(BaseAdmin):
    list_filter = ['name']
    list_display = ['name']
    readonly_fields = base_read_only_fields


class CategoryAdmin(BaseAdmin):
    list_filter = ['name']
    list_display = ['name', 'icon']
    readonly_fields = base_read_only_fields
    inlines = [SubCategoryInline]

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for instance in instances:
            if instance.pk:
                instance.modified_by = request.user
                instance.save()
            else:
                instance.created_by = request.user
                instance.modified_by = request.user
                instance.save()
        formset.save_m2m()

        for deleted in formset.deleted_objects:
            deleted.delete()


class AdPictureInline(admin.TabularInline):
    model = models.AdPicture
    extra = 0
    readonly_fields = base_read_only_fields


class AdPictureAdmin(BaseAdmin):
    list_display = ['image']
    readonly_fields = base_read_only_fields


class AdAdmin(BaseAdmin):
    # fields = ['title', 'description', 'advertiser', 'phone', 'cell']
    list_filter = ['trade_name', 'advertiser', 'phone', 'cell']
    list_display = [
        'title',
        'trade_name',
        'advertiser',
        'phone',
        'cell',
        'state',
        'category'
    ]
    readonly_fields = base_read_only_fields
    inlines = [AdPictureInline]

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for instance in instances:
            if instance.pk:
                instance.modified_by = request.user
                instance.save()
            else:
                instance.created_by = request.user
                instance.modified_by = request.user
                instance.save()
        formset.save_m2m()

        for deleted in formset.deleted_objects:
            deleted.delete()


class ContactAdmin(BaseAdmin):
    list_display = ['address', 'phone', 'email']
    readonly_fields = base_read_only_fields


class AboutAdmin(BaseAdmin):
    list_filter = ['about', 'mission', 'vision', 'values']
    list_display = ['about', 'mission', 'vision', 'values']
    readonly_fields = base_read_only_fields


class CouponAdmin(BaseAdmin):
    list_filter = ['name', 'date', 'active']
    list_display = ['name', 'date', 'coupon', 'active']
    readonly_fields = base_read_only_fields


class BannerInline(admin.TabularInline):
    model = models.Banner
    extra = 0
    min_num = 1
    max_num = 5
    readonly_fields = base_read_only_fields


class BannerAdmin(BaseAdmin):
    list_display = ['banner_group']
    readonly_fields = base_read_only_fields


class BannerGroupAdmin(BaseAdmin):
    list_filter = ['name', 'active']
    list_display = ['name', 'active']
    readonly_fields = base_read_only_fields
    inlines = [BannerInline]
    fieldsets = (
        (None, {
            'fields': ('name', 'active'),
            'description': BANNERS_HELP_TEXT
        }),
    )

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for instance in instances:
            if instance.pk:
                instance.modified_by = request.user
                instance.save()
            else:
                instance.created_by = request.user
                instance.modified_by = request.user
                instance.save()
        formset.save_m2m()

        for deleted in formset.deleted_objects:
            deleted.delete()


class MobileBannerAdmin(BaseAdmin):
    list_display = ['banner_group']
    readonly_fields = base_read_only_fields


class MobileBannerInline(admin.TabularInline):
    model = models.MobileBanner
    extra = 0
    min_num = 1
    max_num = 5
    readonly_fields = base_read_only_fields


class MobileBannerGroupAdmin(BaseAdmin):
    list_filter = ['name', 'active']
    list_display = ['name', 'active']
    readonly_fields = base_read_only_fields
    inlines = [MobileBannerInline]
    fieldsets = (
        (None, {
            'fields': ('name', 'active'),
            'description': BANNERS_HELP_TEXT
        }),
    )

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for instance in instances:
            if instance.pk:
                instance.modified_by = request.user
                instance.save()
            else:
                instance.created_by = request.user
                instance.modified_by = request.user
                instance.save()
        formset.save_m2m()

        for deleted in formset.deleted_objects:
            deleted.delete()


admin.site.register(models.Advertiser, AdvertiserAdmin)
admin.site.register(models.Ad, AdAdmin)
admin.site.register(models.Coupon, CouponAdmin)
admin.site.register(models.Contact, ContactAdmin)
admin.site.register(models.About, AboutAdmin)
admin.site.register(models.Category, CategoryAdmin)
admin.site.register(models.BannerGroup, BannerGroupAdmin)
admin.site.register(models.MobileBannerGroup, MobileBannerGroupAdmin)
