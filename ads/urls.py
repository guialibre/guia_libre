# -*- coding: utf-8 -*-
from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns

import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^contact/', views.contact, name='contact'),
    url(r'^coupons/', views.coupons, name='coupons'),
    url(r'^about/', views.about_us, name='about'),
    url(r'^categories/', views.categories, name='categories'),
    url(r'^search/', views.search, name='search'),
    url(r'^get_categories/', views.get_categories, name='get_categories'),
    url(r'get_states/', views.get_states, name='get_states'),
    url(r'^ads/', views.ads, name='ads'),
    url(r'ad_detail/(?P<pk>[\d]+)$', views.ad_detail, name='ad_detail'),
    url(r'ads_by_category/(?P<pk>[\d]+)$', views.CategoriesListView.as_view(), name='ads_by_category'),
    url(r'^api_ads/$', views.ad_list, name='api_ads'),
    url(r'^api_search_ad/$', views.search_ad, name='api_search_ad'),
    url(r'^api_coupon/$', views.get_coupon, name='api_coupon'),
    url(r'^api_get_info/$', views.get_info, name='api_get_info'),
    url(r'^api_get_categories/$', views.get_categories_api, name='api_get_categories'),
    url(r'^api_most_recent/$', views.get_most_recent, name='api_most_recent'),
    url(r'^api_mobile_banner/$', views.get_mobile_banner, name='api_mobile_banner')

]
urlpatterns = format_suffix_patterns(urlpatterns)
