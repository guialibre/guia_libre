$(document).ready(function () {
    function getCategories() {
        $.ajax({
            type: "GET",
            url: "/get_categories",
            success: function (data) {
                for(var i = 0; i < data.length; i++){
                    $("#categories_select").append($('<option>', {
                        value: data[i].pk,
                        text: data[i].fields.name
                    }))
                }
            },
            error: function (data) {
                console.error(data)
            }
        });
    }

    function getStates() {
        $.ajax({
            type: "GET",
            url: "/get_states",
            success: function (data) {
                for(var i = 0; i < data.length; i++){
                    $("#states_select").append($('<option>', {
                        value: data[i].state,
                        text: data[i].state
                    }))
                }
            },
            error: function (data) {
                console.error(data)
            }
        });
    }

    $('#myCarousel').on('slid.bs.carousel', function() {
        var interval = $('div.item.active').attr('duration');
        setTimeout(function() {
            $('.carousel').carousel('next');
        }, interval);
    });

    function starCarousel() {
        $('#myCarousel').carousel("next");
    }

    starCarousel();

    getStates();
    getCategories();
});